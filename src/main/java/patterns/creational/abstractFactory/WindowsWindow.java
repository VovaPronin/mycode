package patterns.creational.abstractFactory;

public class WindowsWindow implements WindowFactory {


    @Override
    public void createToolBar() {
        WindowsWindowToolBar winToolBar = new WindowsWindowToolBar();
        winToolBar.setTitle("This is Windows Tool Bar");

    }

    @Override
    public void createButtons() {
        WindowsWindowButton winButton = new WindowsWindowButton();
        winButton.setButtonText("This is Windows Button");

    }

    @Override
    public void createText() {
        WindowsWindowText winText = new WindowsWindowText();
        winText.setText("This is Windows Text");

    }


}
