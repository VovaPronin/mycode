package patterns.creational.abstractFactory;

public interface WindowButtons {
    void setButtonText(String text);
}
