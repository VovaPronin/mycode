package patterns.creational.abstractFactory;

public class MacOsWindowToolBar implements WindowToolBar{

    @Override
    public void setTitle(String text) {
        System.out.println(text);

    }
}
