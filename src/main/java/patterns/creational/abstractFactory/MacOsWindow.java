package patterns.creational.abstractFactory;

public class MacOsWindow implements WindowFactory {


    @Override
    public void createToolBar() {
        MacOsWindowToolBar macToolBar = new MacOsWindowToolBar();
        macToolBar.setTitle("This is Mac ToolBar");


    }

    @Override
    public void createButtons() {
        MacOsWindowButton macButton = new MacOsWindowButton();
        macButton.setButtonText("This is Mac Button");

    }

    @Override
    public void createText() {
        MacOsWindowText macText = new MacOsWindowText();
        macText.setText("This is Mac Text");

    }


}
