package patterns.creational.abstractFactory;

public class MacOsWindowButton implements WindowButtons {
    @Override
    public void setButtonText(String text) {
        System.out.println(text);

    }
}
