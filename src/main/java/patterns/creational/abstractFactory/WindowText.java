package patterns.creational.abstractFactory;

public interface WindowText {
    void setText(String text);
}
