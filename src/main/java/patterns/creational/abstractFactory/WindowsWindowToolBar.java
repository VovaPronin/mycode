package patterns.creational.abstractFactory;

public class WindowsWindowToolBar implements WindowToolBar {
    @Override
    public void setTitle(String text) {
        System.out.println(text);

    }
}
