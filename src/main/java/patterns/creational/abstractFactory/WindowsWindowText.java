package patterns.creational.abstractFactory;

public class WindowsWindowText implements WindowText {
    @Override
    public void setText(String text) {
        System.out.println(text);

    }
}
