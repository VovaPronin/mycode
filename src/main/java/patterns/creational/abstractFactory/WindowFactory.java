package patterns.creational.abstractFactory;

public interface WindowFactory {
    void createToolBar();

    void createButtons();

    void createText();


}
