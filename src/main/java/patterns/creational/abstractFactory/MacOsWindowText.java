package patterns.creational.abstractFactory;

public class MacOsWindowText implements WindowText {
    @Override
    public void setText(String text) {
        System.out.println(text);

    }
}
