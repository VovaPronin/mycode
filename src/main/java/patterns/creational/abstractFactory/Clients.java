package patterns.creational.abstractFactory;


public class Clients {

    enum WindowType {
        WIN, MAC
    }

    public static void main(String[] args) {
        Clients client = new Clients();
        WindowFactory windowFactory = client.getWindowFactory(WindowType.MAC);
        windowFactory.createButtons();
        windowFactory.createText();
        windowFactory.createToolBar();
        System.out.println(client);


    }

    public WindowFactory getWindowFactory(WindowType windowType) {
        switch (windowType) {
            case MAC:
                return new MacOsWindow();
            case WIN:
                return new WindowsWindow();
            default:
                return null;
        }

    }

}
