package patterns.creational.abstractFactory;

public class WindowsWindowButton implements WindowButtons {
    @Override
    public void setButtonText(String text) {
        System.out.println(text);

    }
}
