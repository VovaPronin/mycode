package patterns.creational.abstractFactory;

public interface WindowToolBar {
    void setTitle(String text);
}
