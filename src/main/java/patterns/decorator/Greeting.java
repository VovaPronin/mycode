package patterns.decorator;

public interface Greeting {

    String greeting();
}
