package patterns.decorator;

public class Sample {

    public static void main(String[] args) {
        Sample sample = new Sample();
        sample.hashCode();
        System.out.println(new GoodGreeting(new SaratovGreeting(new StandatrGreeting())).greeting());

    }
}
