package patterns.decorator;

public class SaratovGreeting implements Greeting{

    Greeting greeting;

    public SaratovGreeting(Greeting greeting) {
        this.greeting = greeting;
    }

    @Override
    public String greeting() {
        return greeting.greeting() + ", ebta!!";
    }
}
