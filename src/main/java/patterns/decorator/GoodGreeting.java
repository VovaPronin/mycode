package patterns.decorator;

public class GoodGreeting implements Greeting {

    private Greeting greeting;

    public GoodGreeting(Greeting greeting) {
        this.greeting = greeting;
    }

    @Override
    public String greeting() {
        return greeting.greeting() + ", pozaluista";
    }
}
