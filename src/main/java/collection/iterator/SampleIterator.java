package collection.iterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.ListIterator;

public class SampleIterator {
    public static void main(String[] args) {
        //Создаем лист
        ArrayList<String> al = new ArrayList<>();
        //Добавим элементы
        al.add("Vova");
        al.add("Sasha");
        al.add("Dima");
        al.add("Petia");
        al.add("Kolia");
        //пробежимся по всем объектам
        System.out.println("Elements in List: ");
        Iterator<String> iter = al.iterator();
        while (iter.hasNext()) {
            String element = iter.next();
            System.out.print(element + " ");
        }
             //добавим элементы
        ListIterator<String> liter = al.listIterator();
        while (liter.hasNext()){
            String element = liter.next();
            liter.set(element + "+");
        }

        System.out.println(al);
    }
}
