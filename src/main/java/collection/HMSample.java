package collection;

import java.util.HashMap;

public class HMSample {

    public static void main(String[] args) {
        HashMap<String, String> map = new HashMap<>();
        map.put("house", "1");
        map.put("flat", "24");
        map.put("flat", "26");

        System.out.println(map.get("flat"));
    }
}
