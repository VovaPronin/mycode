package collection.linkedList;

public class ArrayList {
    private Item head = null;
    private int size = 0;

    public void add(Object newObject) {
        if (head == null) {
            head = new Item();
            head.value = newObject;

        } else {
            Item current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = new Item();
            current.next.value = newObject;
            current.next.previos = current;
        }
        size ++;

    }


    public Object get(int index) {
        if(index > size){
            throw new ArrayIndexOutOfBoundsException();
        }
        if(head == null){
            return null;
        }
        int currentIndex = 0;
        Item current = head;
        while(currentIndex < index){
            current = current.next;
            currentIndex++;
        }
        return current;
    }

    public void delete(int index) {
        if(index > size){
            throw new ArrayIndexOutOfBoundsException();
        }
        if(head == null){
            throw new ArrayIndexOutOfBoundsException();

        }
        int currentIndex = 0;
        Item current = head;
        while(currentIndex < index){
            current = current.next;
            currentIndex++;
        }
        Item previos = current.previos;
        Item next = current.next;
        previos.next = next;
        next.previos = previos;


    }

    public int getSize() {
        return size;
    }

    class Item {
        Item previos;
        Item next;
        Object value;


        public Item getPrevios() {
            return previos;
        }

        public void setPrevios(Item previos) {
            this.previos = previos;
        }

        public Item getNext() {
            return next;
        }

        public void setNext(Item next) {
            this.next = next;
        }
    }
}

